<?php

namespace App\Http\Controllers;

use App\Custom\LatLonDistance;
use Illuminate\Http\Request;
use DB;

class HelperController extends Controller {
    public function undo() {
        DB::table('crawled_data')->chunk(100, function($data) {
            foreach ($data as $listing) {
                $listing = json_decode(json_encode($listing), true);
                $lat = explode(',', $listing["lat_lon"])[0];
                DB::table('crawled_data')->where('data_id', $listing["data_id"])->update(['latitude' => $lat]);
            }
        });
    }

    public function getImages() {
        $path = getcwd() . "/avatarImages/";
        if (!file_exists($path)) {
            mkdir($path);
        }

        function saveImage($in, $out) {
            $in = @fopen($in, "rb");

            if ($in) {
                $out = fopen($out, "wb");
                while ($chunk = fread($in, 8192)) {
                    fwrite($out, $chunk, 8192);
                }
                fclose($in);
                fclose($out);
            } else {
                echo("Failed to open resource");
            }
        }

        DB::table('crawled_data')->chunk(100, function($data) {
            $path = getcwd() . "/avatarImages/";

            foreach ($data as $listing) {
                $listing = json_decode(json_encode($listing), true);
                $avatar = $listing["avatar_url"];

                if (strpos($avatar, "/") !== 0) {
                    saveImage($avatar, $path . $listing["id"] . ".jpg");
                }
            }
        });
    }

    public function insertEmails() {
        $path = getcwd() . "\\out.csv";
        $lines = [];
        $handle = fopen($path, "r");

        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                array_push($lines, $line);
            }

            fclose($handle);

            foreach ($lines as $line) {
                $exploded = explode(",", $line);
                $url = str_replace("\"", "", $exploded[0]);
                $url = str_replace("https://weedmaps.com/dispensaries/", "", $url);
                $email = str_replace("\"", "", $exploded[1]);
                $email = str_replace("\\n", "", $email);

                DB::table('crawled_data')
                    ->where("slug", $url)
                    ->update(["email_address" => $email]);
            }
        } else {
            echo("Failed to open file at: " . $path);
        }
    }

    public function exportEmailNames() {
        function done($data) {
            $out = fopen(getcwd() . "\\out.txt", "a");

            foreach ($data as $item) {
                fwrite($out, $item, 8192);
            }

            fclose($out);
        }

        DB::table('crawled_data')->chunk(100, function($data) {
            $arr = [];

            foreach ($data as $listing) {
                $listing = json_decode(json_encode($listing), true);
                $url = "http://dispensarylocation.com/listing/" . $listing["slug"];
                $phone = $listing["phone_number"];

                $email = $listing["email_address"];
                $email = str_replace("\n", "", $email);
                $email = $email == "Tbd@gmail.com" ? "" : $email;

                $csv = "\"$url\",\"$phone\",\"$email\"" . PHP_EOL;
                array_push($arr, $csv);
            }

            done($arr);
        });
    }

    public function getAddresses() {
        $in = getcwd() . "\\slugs.txt";

        $contents = file_get_contents($in);
        $slugs = explode("\r\n", $contents);
        $states = [];

        foreach ($slugs as $slug) {
            $slug = str_replace("\r\n", "", $slug);
            $query = "SELECT * FROM crawled_data WHERE slug='$slug'";
            $results = DB::select($query);

            if (count($results) > 0) {
                //$state = $results[0]->state;
                $addr = $results[0]->address;
                array_push($states, $addr);
            }
        }

        $out = fopen(getcwd() . "\\out.txt", "wb");
        foreach ($states as $state) {
            fwrite($out, $state . "\r\n", 8192);
        }

        fclose($out);
    }

    public function getMenuBodyName() { 
        set_time_limit(0);
        DB::table('menu_items')
            ->where('id', '>', 194255)
            ->chunk(500, function($data) {
                foreach ($data as $item) {
                    $item = json_decode(json_encode($item), true);
                    $remoteJSON = @file_get_contents("https://weedmaps.com/api/web/v1/listings/" . $item["listing_slug"] . "/menu?type=dispensary");
                    if ($remoteJSON !== FALSE) {
                        $jsonOBJ = json_decode($remoteJSON, true);

                        foreach ($jsonOBJ["categories"] as $category) {
                            foreach ($category["items"] as $categoryItem) {
                                if ($categoryItem["slug"] == $item["item_slug"]) { //Both remote and local slug match
                                    DB::table('menu_items')
                                        ->where('item_slug', $categoryItem["slug"])
                                        ->where('listing_slug', $item["listing_slug"])
                                        ->update([
                                            "body" => $categoryItem["body"],
                                            "name" => $categoryItem["name"],
                                            "license" => $categoryItem["license_type"]
                                        ]);
                                }
                            }
                        }
                    }
                }
            });
    }
}