<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class VerifyAdmin {
    public function handle($request, Closure $next) {
        if (Auth::check() && Auth::user()->level == 1) { //Is logged in & admin
            return $next($request);
        } else {
            return redirect('/');
        }
    }
}