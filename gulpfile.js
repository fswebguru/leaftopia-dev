const elixir = require('laravel-elixir');

require('laravel-elixir-vue');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('elements/popover.sass', './public/assets/css/popover.css')
        .sass('pages/business.sass', './public/assets/css/business.css')
        .sass(['pages/profile.sass', 'variables.sass'], './public/assets/css/profile.css')
        .sass(['style.sass', 'pages/listing.sass', 'variables.sass', 'pages/home.sass', 'pages/map.sass', 'pages/business.sass', 'pages/profile.sass'], './public/assets/css/style.css')
        .webpack('app.js');
});
