$(document).ready(function() {
    $(".accordion button").click(function() {
        var $toSlide = $(this).closest(".accordion").find(".panel")

        if ($toSlide.is(":hidden")) {
            $toSlide.slideDown(300);
        } else {
            $toSlide.slideUp(300);
        }
    });
});